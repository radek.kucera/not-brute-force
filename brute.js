/**
 * Everything but not brute forcee :)
 * @param {number} from - Beginning number of searching characters. (2 - aa, ab, ...)
 * @param {number} to - Ending number of searching characters. (3 - ..., zzy, zzz)
 * @param {HTMLelement} inputElement - Element representing input for being filled with string.
 * @param {HTMLelement} submitElement - Element representing button to being clicked after filling.
 */
function notBruteForceShh(from, to, inputElement, submitElement) {
  let fromString = "", toString = "";

  for (let i = 0; i < to; i++) {
    if (i < from) {
      fromString += "a";
    }
    toString += "z";
  }

  for (let i = parseInt(fromString, 36); i <= parseInt(toString, 36); i++) {
    inputElement.value = i.toString(36);
    submitElement.click();
  }
}